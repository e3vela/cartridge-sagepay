from __future__ import unicode_literals
import binascii

from Crypto.Cipher import AES

from django.core.urlresolvers import reverse
from django.http import Http404
from django.shortcuts import get_object_or_404, render, redirect
from django.utils.datastructures import MultiValueDictKeyError
from django.views.decorators.cache import never_cache

from mezzanine.conf import settings
from mezzanine.utils.importing import import_dotted_path
from mezzanine.utils.views import set_cookie

from cartridge.shop import checkout
from cartridge.shop.models import ProductVariation, Order
from cartridge.shop.forms import OrderForm
from cartridge.shop.utils import sign

try:
    from xhtml2pdf import pisa
except (ImportError, SyntaxError):
    pisa = None
HAS_PDF = pisa is not None


# Set up checkout handlers.
handler = lambda s: import_dotted_path(s) if s else lambda *args: None
billship_handler = handler(settings.SHOP_HANDLER_BILLING_SHIPPING)
tax_handler = handler(settings.SHOP_HANDLER_TAX)
payment_handler = handler(settings.SHOP_HANDLER_PAYMENT)
order_handler = handler(settings.SHOP_HANDLER_ORDER)


@never_cache
def checkout_steps(request, form_class=OrderForm, extra_context=None):
    """
    Display the order form and handle processing of each step.
    """

    # Do the authentication check here rather than using standard
    # login_required decorator. This means we can check for a custom
    # LOGIN_URL and fall back to our own login view.
    authenticated = request.user.is_authenticated()
    if settings.SHOP_CHECKOUT_ACCOUNT_REQUIRED and not authenticated:
        url = "%s?next=%s" % (settings.LOGIN_URL, reverse("shop_checkout"))
        return redirect(url)

    try:
        settings.SHOP_CHECKOUT_FORM_CLASS
    except AttributeError:
        pass
    else:
        from warnings import warn
        warn("The SHOP_CHECKOUT_FORM_CLASS setting is deprecated - please "
             "define your own urlpattern for the checkout_steps view, "
             "passing in your own form_class argument.")
        form_class = import_dotted_path(settings.SHOP_CHECKOUT_FORM_CLASS)

    initial = checkout.initial_order_data(request, form_class)
    step = int(request.POST.get("step", None) or
               initial.get("step", None) or
               checkout.CHECKOUT_STEP_FIRST)
    form = form_class(request, step, initial=initial)
    data = request.POST
    checkout_errors = []

    if request.POST.get("back") is not None:
        # Back button in the form was pressed - load the order form
        # for the previous step and maintain the field values entered.
        step -= 1
        form = form_class(request, step, initial=initial)
    elif request.method == "POST" and request.cart.has_items():
        form = form_class(request, step, initial=initial, data=data)
        if form.is_valid():
            # Copy the current form fields to the session so that
            # they're maintained if the customer leaves the checkout
            # process, but remove sensitive fields from the session
            # such as the credit card fields so that they're never
            # stored anywhere.
            request.session["order"] = dict(form.cleaned_data)
            sensitive_card_fields = ("card_number", "card_expiry_month",
                                     "card_expiry_year", "card_ccv")
            for field in sensitive_card_fields:
                if field in request.session["order"]:
                    del request.session["order"][field]

            # FIRST CHECKOUT STEP - handle shipping and discount code.
            if step == checkout.CHECKOUT_STEP_FIRST:
                # Discount should be set before shipping, to allow
                # for free shipping to be first set by a discount
                # code.
                form.set_discount()
                try:
                    billship_handler(request, form)
                    tax_handler(request, form)
                except checkout.CheckoutError as e:
                    checkout_errors.append(e)

            # FINAL CHECKOUT STEP - handle payment and process order.
            if step == checkout.CHECKOUT_STEP_LAST and not checkout_errors:
                # Create and save the initial order object so that
                # the payment handler has access to all of the order
                # fields. If there is a payment error then delete the
                # order, otherwise remove the cart items from stock
                # and send the order receipt email.
                order = form.save(commit=False)
                order.setup(request)
                # Try payment.
                try:
                    transaction_id = payment_handler(request, form, order)
                except checkout.CheckoutError as e:
                    # Error in payment handler.
                    order.delete()
                    checkout_errors.append(e)
                    if settings.SHOP_CHECKOUT_STEPS_CONFIRMATION:
                        step -= 1
                else:
                    # Finalize order - ``order.complete()`` performs
                    # final cleanup of session and cart.
                    # ``order_handler()`` can be defined by the
                    # developer to implement custom order processing.
                    # Then send the order email to the customer.
                    order.transaction_id = transaction_id
                    order.complete(request)
                    order_handler(request, form, order)
                    # checkout.send_order_email(request, order)
                    # Set the cookie for remembering address details
                    # if the "remember" checkbox was checked.
                    response = redirect("shop_complete")
                    if form.cleaned_data.get("remember"):
                        remembered = "%s:%s" % (sign(order.key), order.key)
                        set_cookie(response, "remember", remembered,
                                   secure=request.is_secure())
                    else:
                        response.delete_cookie("remember")
                    return response

            # If any checkout errors, assign them to a new form and
            # re-run is_valid. If valid, then set form to the next step.
            form = form_class(request, step, initial=initial, data=data,
                              errors=checkout_errors)
            if form.is_valid():
                step += 1
                form = form_class(request, step, initial=initial)

    # Update the step so that we don't rely on POST data to take us back to
    # the same point in the checkout process.
    try:
        request.session["order"]["step"] = step
        request.session.modified = True
    except KeyError:
        pass

    step_vars = checkout.CHECKOUT_STEPS[step - 1]
    template = "shop/%s.html" % step_vars["template"]
    context = {"CHECKOUT_STEP_FIRST": step == checkout.CHECKOUT_STEP_FIRST,
               "CHECKOUT_STEP_LAST": step == checkout.CHECKOUT_STEP_LAST,
               "CHECKOUT_STEP_PAYMENT": (settings.SHOP_PAYMENT_STEP_ENABLED and
                                         step == checkout.CHECKOUT_STEP_PAYMENT),
               "step_title": step_vars["title"], "step_url": step_vars["url"],
               "steps": checkout.CHECKOUT_STEPS, "step": step, "form": form}
    context.update(extra_context or {})
    return render(request, template, context)


@never_cache
def complete(request, template="shop/complete.html", extra_context=None):
    """
    Redirected to once an order is complete - pass the order object
    for tracking items via Google Anayltics, and displaying in
    the template if required.
    """

    def aes_pad(crypt):
        padding = ""
        padlength = 16 - (len(crypt.encode('utf-8')) % 16)
        for _i in range(1, padlength + 1):
            padding += chr(padlength)
        return (crypt + padding).encode('utf-8')

    def aes_enc(enckey, data):
        aes = AES.new(enckey, AES.MODE_CBC, enckey)
        data = aes_pad(data)
        enc = aes.encrypt(data)
        enc = b"@" + binascii.hexlify(enc)
        return enc

    try:
        order = Order.objects.from_request(request)
    except Order.DoesNotExist:
        raise Http404
    items = order.items.all()
    # Assign product names to each of the items since they're not
    # stored.
    skus = [item.sku for item in items]
    variations = ProductVariation.objects.filter(sku__in=skus)
    names = {}
    for variation in variations.select_related("product"):
        names[variation.sku] = variation.product.title
    for i, item in enumerate(items):
        setattr(items[i], "name", names[item.sku])
    data = {
        'VendorTxCode': order.pk,
        'Amount': "%.2f" % (order.total,),
        'Currency': settings.SAGEPAY_CURRENCY_CODE,
        'Description': "Order #%s" % (order.pk,),
        'SuccessURL': "http" + ("s" if settings.SAGEPAY_USE_HTTPS else "") +
        "://" + request.get_host() + reverse("shop_sagepay"),
        'FailureURL': "http" + ("s" if settings.SAGEPAY_USE_HTTPS else "") +
        "://" + request.get_host() + reverse("shop_sagepay"),
        'BillingSurname': order.billing_detail_last_name,
        'BillingFirstnames': order.billing_detail_first_name,
        'BillingAddress1': order.billing_detail_street,
        'BillingCity': order.billing_detail_city,
        'BillingPostCode': order.billing_detail_postcode,
        'BillingCountry': order.billing_detail_country,
        'DeliverySurname': order.shipping_detail_last_name,
        'DeliveryFirstnames': order.shipping_detail_first_name,
        'DeliveryAddress1': order.shipping_detail_street,
        'DeliveryCity': order.shipping_detail_city,
        'DeliveryPostCode': order.shipping_detail_postcode,
        'DeliveryCountry': order.shipping_detail_country}
    udata = "&".join("%s=%s" % kv for kv in data.items())
    crypt = aes_enc(settings.SAGEPAY_ENCRYPTION_KEY, udata).upper()
    context = {"order": order, "items": items, "has_pdf": HAS_PDF,
               "steps": checkout.CHECKOUT_STEPS,
               'vpsprotocol': "3.00",
               'txtype': 'PAYMENT',
               'vendor': settings.SAGEPAY_VENDOR,
               'crypt': crypt}
    context.update(extra_context or {})
    return render(request, template, context)


@never_cache
def sagepay(request, template="shop/sagepay.html"):
    """
    Redirected to once an order is complete - pass the order object
    for tracking items via Google Anayltics, and displaying in
    the template if required.
    """

    def aes_dec(enckey, data):
        try:
            data = data.lstrip(b'@').decode('utf-8')
            aes = AES.new(enckey, AES.MODE_CBC, enckey)
            dec = binascii.unhexlify(data)
            dec = aes.decrypt(dec)
        except TypeError:
            raise Http404
        return dec

    try:
        udata = aes_dec(settings.SAGEPAY_ENCRYPTION_KEY, request.GET['crypt'])
        data = {}
        for kv in udata.split('&'):
            k, v = kv.split('=')
            data[k] = v
    except (ValueError, UnicodeDecodeError, MultiValueDictKeyError):
        raise Http404
    order = get_object_or_404(Order, pk=data["VendorTxCode"])
    # Check if order is pending
    if order.status == settings.SHOP_ORDER_STATUS_CHOICES[0][0]:
        if data['Status'] == 'OK':
            order.status = settings.SHOP_ORDER_STATUS_CHOICES[1][0]
            order.save()
        else:
            order.status = settings.SHOP_ORDER_STATUS_CHOICES[2][0]
            order.save()
    checkout.send_order_email(request, order)
    return render(request, template,
                  {'order': order,
                   'status_detail': data["StatusDetail"]})
