from __future__ import unicode_literals

from django.conf.urls import patterns, url

from mezzanine.conf import settings


_slash = "/" if settings.APPEND_SLASH else ""

urlpatterns = patterns("sagepay.views",
    url("^checkout%s$" % _slash, "checkout_steps", name="shop_checkout"),
    url("^checkout/complete%s$" % _slash, "complete", name="shop_complete"),
    url("^checkout/sagepay%s$" % _slash, "sagepay", name="shop_sagepay"),
)
