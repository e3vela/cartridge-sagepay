from __future__ import unicode_literals

from mezzanine.conf import register_setting

register_setting(
    name="SAGEPAY_VENDOR",
    label="Sagepay account vendor",
    default="",
    editable=True,
)

register_setting(
    name="SAGEPAY_USERNAME",
    label="Sagepay account username",
    default="",
    editable=True,
)

register_setting(
    name="SAGEPAY_ENCRYPTION_KEY",
    label="Sagepay encryption key",
    default="",
    editable=True,
)

register_setting(
    name="SAGEPAY_USE_HTTPS",
    label="Sagepay use https",
    description="Use https protocol for the succesfull and failure URLs.",
    default=False,
    editable=False,
)

register_setting(
    name="SAGEPAY_CURRENCY_CODE",
    label="Sagepay currency code",
    description="The currency code you are using in the store.",
    default="GBP",
    editable=True,
)
